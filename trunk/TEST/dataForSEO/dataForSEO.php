<?php
require('RestClient.php');
//You can download this file from here https://api.dataforseo.com/_examples/php/_php_RestClient.zip

try {
    //Instead of 'login' and 'password' use your credentials from https://my.dataforseo.com/login
    $client = new RestClient('https://api.dataforseo.com', null, 'exe.harish@gmail.com', 'lX4Up2uB7uEAiNzc');

} catch (RestClientException $e) {
    echo "\n";
    print "HTTP code: {$e->getHttpCode()}\n";
    print "Error code: {$e->getCode()}\n";
    print "Message: {$e->getMessage()}\n";
    print  $e->getTraceAsString();
    echo "\n";
}

/*
#1 - get ALL ready results
recommended use of getting results:
run this script by cron with 10-60 streams, every minute with random delay 0-30 sec.
usleep(mt_rand(0,30000000));
*/

try {
    //GET /v2/rnk_tasks_get
    $task_get_result = $client->get('v2/rnk_tasks_get');
    echo "<pre>"; print_r($task_get_result); echo "</pre>";

    //do something with results

} catch (RestClientException $e) {
    echo "<pre>";
    print "HTTP code: {$e->getHttpCode()}\n";
    print "Error code: {$e->getCode()}\n";
    print "Message: {$e->getMessage()}\n";
    print  $e->getTraceAsString();
    echo "</pre>";
}


/*
#2 - get one result by task_id
*/
try {

    // GET /api/v1/tasks_get/$task_id
    $task_get_result = $client->get('v2/rnk_tasks_get/123456789');
    echo "<pre>"; print_r($task_get_result); echo "</pre>";

    //do something with result

} catch (RestClientException $e) {
    echo "<pre>";
    print "HTTP code: {$e->getHttpCode()}\n";
    print "Error code: {$e->getCode()}\n";
    print "Message: {$e->getMessage()}\n";
    print  $e->getTraceAsString();
    echo "</pre>";
}

$client = null;
?>