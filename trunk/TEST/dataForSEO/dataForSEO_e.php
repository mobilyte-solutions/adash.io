<?php
require('RestClient.php');
//You can download this file from here https://api.dataforseo.com/_examples/php/_php_RestClient.zip

try {
    //Instead of 'login' and 'password' use your credentials from https://my.dataforseo.com/login
    $client = new RestClient('https://api.dataforseo.com/', null, 'exe.harish@gmail.com', 'lX4Up2uB7uEAiNzc');

} catch (RestClientException $e) {
    
    echo "<pre>";
    print "HTTP code: {$e->getHttpCode()}\n";
    print "Error code: {$e->getCode()}\n";
    print "Message: {$e->getMessage()}\n";
    print  $e->getTraceAsString();
    echo "</pre>";
    exit();
}

$post_array = array();

//for example, some data selection cycle for tasks
for ($i = 0; $i < 3; $i++) {

    // example #1 - the simplest one
    // you set only a website URL and a search engine URL.
    // This search engine URL string will be searched, compared to our internal parameters
    // and used as:
    // "se_id", "loc_id", "key_id" ( actual and fresh list can be found here: "se_id":
    // https://api.dataforseo.com/v2/cmn_se , "loc_id": https://api.dataforseo.com/v2/cmn_locations ) (see example #3 for details)
    // If a task was set successfully, this *_id will be returned in results: 'v2/rnk_tasks_post' so you can use it.
    // The setting of a task can fail, if you set not-existent search engine, for example.
    // Disadvantages: You cannot work with "map pack", "maps", "mobile"

    /*$my_unq_id = mt_rand(0,30000000); //your unique ID. We will return it with all results
    $post_array[$my_unq_id] = array(
        "priority" => 1,
        "site" => "dataforseo.com",
        "url" => "https://www.google.co.uk/search?q=seo%20data%20api&hl=en&gl=GB&uule=w+CAIQIFISCXXeIa8LoNhHEZkq1d1aOpZS"
    );*/

    // example #2 - will return results faster than #1, but is simpler than example #3
    // All parameters should be set in the text format.
    // All data will will be searched, compared to our internal parameters
    // and used as:
    // "se_id", "loc_id", "key_id" ( actual and fresh list can be found here: "se_id": https://api.dataforseo.com/v2/cmn_se ,
    // "loc_id": https://api.dataforseo.com/v2/cmn_locations )
    // If a task was set successfully, this *_id will be returned in results: 'v2/rnk_tasks_post' so you can use it.
    // The setting of a task can fail, if you set not-existent search engine, for example.
    // Disadvantages: The process of search and comparison of provided data to our internal parameters may take some time.
    $my_unq_id = mt_rand(0,30000000); //your unique ID. will be returned with all results

    $post_array[$my_unq_id] = array(
        "priority" => 1,
        "site" => "51blocks.com",
        "se_name" => "google.com",
        "se_language" => "English",
        "loc_name_canonical"=> "United States",
        "key" => mb_convert_encoding("private label seo services", "UTF-8")
    );

    // example #3 - the fastest one. All parameters should be set in our internal format.
    // Actual and fresh list can be found here: "se_id": https://api.dataforseo.com/v2/cmn_se ,
    // "loc_id": https://api.dataforseo.com/v2/cmn_locations
    /*$my_unq_id = mt_rand(0,30000000); //your unique ID. We will return it with all results
    $post_array[$my_unq_id] = array(
    "priority" => 1,
    "site" => "dataforseo.com",
    "se_id" => 22,
    "loc_id" => 1006886,
    "key_id" => 62845222
    );*/

    //This example has a cycle of up to 3 elements, but in the case of large number of tasks - send up to 100 elements per POST request
    if (count($post_array) > 99) {
        try {
            // POST /v2/rnk_tasks_post/$data
            // $tasks_data must by array with key 'data'
            $task_post_result = $client->post('v2/rnk_tasks_post', array('data' => $post_array));
            
            echo "<pre>"; print_r($task_post_result); echo "</pre>";

            //do something with post results

            $post_array = array();
        } catch (RestClientException $e) {
            
            echo "<pre>";
            print "HTTP code: {$e->getHttpCode()}\n";
            print "Error code: {$e->getCode()}\n";
            print "Message: {$e->getMessage()}\n";
            print  $e->getTraceAsString();
            echo "</pre>";
        }
    }
}

if (count($post_array) > 0) {
    try {
        // POST /v2/rnk_tasks_post/$data
        // $tasks_data must by array with key 'data'
        $task_post_result = $client->post('v2/rnk_tasks_post', array('data' => $post_array));
        
        echo "<pre>"; print_r($task_post_result); echo "</pre>";

        //do something with post results

    } catch (RestClientException $e) {
        
        echo "<pre>";
        print "HTTP code: {$e->getHttpCode()}\n";
        print "Error code: {$e->getCode()}\n";
        print "Message: {$e->getMessage()}\n";
        print  $e->getTraceAsString();
        echo "</pre>";
    }
}

$client = null;

die();
?>