<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Glossary extends MY_Controller {

	public function __construct() {
		parent::__construct();
		if (!in_array($this->router->method,
			array('viewPublicSocialReport'))) {
			$this->isLogin();
		}

	}
	public function index(){
		redirect("/glossary/report");
	}

	public function report($publicView = 0) {
		if ($publicView) {
			$log_user_id = $prodDet['account_id'];
		} else {
			$log_user_id = com_user_data('id');
			if ($log_user_id != $prodDet['account_id']) {
				$log_user_id = $prodDet['account_id'];
				// redirect('accounts/ulist');
				// exit;
			}
		}
		$inner = array();
		$shell = array();
		$log_user_id = com_user_data('id');
		$shell['page_title'] = 'Report';
		$shell['content'] = $this->load->view('report', $inner, true);
		$temp = TMP_DEFAULT;
		if ($publicView) {
			$temp = TMP_PREPORT;
		}
		$this->load->view($temp, $shell);
	}
	public function metrics($publicView = 0) {
		if ($publicView) {
			$log_user_id = $prodDet['account_id'];
		} else {
			$log_user_id = com_user_data('id');
			if ($log_user_id != $prodDet['account_id']) {
				$log_user_id = $prodDet['account_id'];
				// redirect('accounts/ulist');
				// exit;
			}
		}
		$inner = array();
		$shell = array();
		$log_user_id = com_user_data('id');
		$shell['page_title'] = 'Metrics';
		$shell['content'] = $this->load->view('metrics', $inner, true);
		$temp = TMP_DEFAULT;
		if ($publicView) {
			$temp = TMP_PREPORT;
		}
		$this->load->view($temp, $shell);
	}

	

}