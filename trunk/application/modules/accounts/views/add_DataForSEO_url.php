<style type="text/css">
  .customTool{
    font-style: normal;
    padding: 0 5px;    
  }
  .tooltip-content {
    font-size: 11px;
    line-height: normal;
    padding: 10px;
}
</style>

<form action="<?= base_url( 'accounts/add_DataForSEO_url/'.$account_details['id'] ); ?>" id="add_DataForSEO_url" name="addProfile" method="POST" enctype="multipart/form-data">

    <?php 
      
      //echo "<pre>"; print_r($account_details); echo "</pre>"; 

      if($this->session->flashdata('res_status')){ 
        echo "<div class='alert alert-".$this->session->flashdata('res_status')."'>".$this->session->flashdata('res_message')."</div>"; 
      }

      $url_x_http_s = preg_replace("(^https?://)", "", $account_details['account_url'] );
      $url_x_www = preg_replace('#^www\.(.+\.)#i', '$1', $url_x_http_s);
      $url = rtrim($url_x_www, "/");

    ?>

    <input type="hidden" name="account_url_profiles_id" value="<?=$account_details['id']?>">

    <div class="form-group row">
      <label for="site" class="col-2 col-form-label">site</label>
      <div class="col-10">
        <input class="form-control" type="text" value="<?=$url?>" id="site" name="site" readonly>
      </div>
    </div>
    <div class="form-group row">
      <label for="site" class="col-2 col-form-label">Search engine name</label>
      <div class="col-10">
        <input class="form-control" type="text" value="google.com" id="se_name" name="se_name" readonly>
      </div>
    </div>
    <div class="form-group row">
      <label for="se_language" class="col-2 col-form-label">Searche Language</label>
      <div class="col-10">
        <input class="form-control" type="text" value="English" id="se_language" name="se_language" readonly>
      </div>
    </div>
    <div class="form-group row">
      <label for="loc_name_canonical" class="col-2 col-form-label">Target Location</label>
      <div class="col-10">
        <input class="form-control" type="text" value="United States" id="loc_name_canonical" name="loc_name_canonical" readonly>
      </div>
    </div>
    <div class="form-group row">
      <label for="search_keyword" class="col-2 col-form-label">Keyword</label>
      <div class="col-10">
        <input class="form-control" type="text" value="" id="search_keyword" name="search_keyword" required>
      </div>
    </div>

    <button type="submit" class="btn btn-success waves-effect waves-light float-right">Add Keyword</button>

</form>