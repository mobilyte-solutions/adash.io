<style type="text/css">
  .customTool{
    font-style: normal;
    padding: 0 5px;    
  }
  .tooltip-content {
    font-size: 11px;
    line-height: normal;
    padding: 10px;
}
</style>


<div class="row">
    <div class="col-12">
        <div class="card">

            <div class="card-body">

              <div class="row">
                <div class="col-md-6 col-8 align-self-center">
                    <h4 class="card-title">Accounts Keywords</h4>
                </div>
                <div class="col-md-6 col-4 align-self-center">

                    <?php $account_d = $this->uri->segment(3); ?>

                    <a href="<?= base_url( 'accounts/add_DataForSEO_url/'.$account_d )?>" class="btn pull-right btn-info">Add Keyword</a>
                </div>
              </div>

              <hr>

              <h4 class="card-title">KPIs</h4>
              <!-- <h6 class="card-subtitle">Create responsive tables by wrapping any <code>.table</code> in <code>.table-responsive </code></h6> -->
              <div class="table-responsive">
                  <table class="table">
                      <thead>
                          <tr>
                              <th>Total Keywords in Top 3</th>
                              <th>Total Keywords in Top 10</th>
                              <th>Total Keywords in Top 20</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td>10</td>
                              <td>20</td>
                              <td>15</td>
                          </tr>
                      </tbody>
                  </table>
              </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
  <div class="col-12">
      <div class="card">
          <div class="card-body">
              <!-- <h4 class="card-title">Responsive Table </h4>
              <h6 class="card-subtitle">Create responsive tables by wrapping any <code>.table</code> in <code>.table-responsive </code></h6> -->
              <div class="table-responsive">
                  <table class="table">
                      <thead>
                          <tr>
                              <th>Chart Icon</th>
                              <th>Keyword</th>
                              <th>URL</th>
                              <th>Type</th>
                              <th>Current Rank</th>
                              <th>Change</th>
                              <th>First Rank</th>
                              <th>All-Time Increase/Descrease</th>
                              <th>Keyword Volume</th>
                              <th>CPC</th>
                              <th>Competiton Level</th>
                              <th>Search Volume Trend</th>
                          </tr>
                      </thead>
                      <tbody>

                          <tr>
                              <td>Icon</td>
                              <td>white label seo</td>
                              <td> 
                                <a href="http://www.51blocks.com/whitelableseo">http://www.51blocks.com/whitelableseo</a> 
                              </td>
                              <td>(Organic, Local...etc)</td>
                              <td>6</td>
                              <td>+1</td>
                              <td>12</td>
                              <td>+6</td>
                              <td>5589</td>
                              <td>$5.58</td>
                              <td>0.78</td>
                              <td></td>
                          </tr>

                          <tr>
                              <td>Icon</td>
                              <td>denver seo</td>
                              <td> 
                                <a href="www.51bloks.com">http://www.51bloks.com</a> 
                              </td>
                              <td>(Organic, Local...etc)</td>
                              <td>14</td>
                              <td>-56</td>
                              <td>20</td>
                              <td>+6</td>
                              <td>2243</td>
                              <td>$2.98</td>
                              <td>0.85</td>
                              <td></td>
                          </tr>
                          
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>

<!-- <form action="<?= base_url( 'accounts/addProfileUrl' ); ?>" 
      id="addProfile" name="addProfile" 
      method="POST"
      enctype="multipart/form-data" >
    <div class="form-group row">
      <label for="name" class="col-2 col-form-label">Account Url</label>
      <div class="col-10">
        <input class="form-control" type="text" value="" id="account_url" required name="account_url">
      </div>
    </div>
    <div class="form-group row">      
      <label for="name" class="col-2 col-form-label">
        Close Rate <i class="mytooltip mdi mdi-tooltip-text">
          <span class="tooltip-content customTool text-white">Close rate - Your closing ratio is the number of sales you close compared to the number of leads. Say, for instance, you have 10 last month and closed 2 sales as a result. You closed 2/10ths, or 20 percent, of your potential sales.</span></i>
        </i>
      </label>
      <div class="col-2">
        <input  class="form-control"  style="width: 90%"
                type="number" value="0.00" min="0" value="0" step=".01"
                id="close_rate" required name="close_rate"><span>%</span>
      </div>
      <div class="col-8"></div>
    </div>
    <div class="form-group row">
      <label for="name" class="col-2 col-form-label">Avg. Sale Amt.
        <i class="mytooltip mdi mdi-tooltip-text">
          <span class="tooltip-content customTool text-white">Avg sales amount - this is a value in dollars that 1 sale is worth.</span>
          </i>
      </label>
      <div class="col-2">
        <span>$</span>
        <input  class="form-control"  style="width: 90%"
                type="number" value="0.00" min="0" value="0" step=".01"
                id="avg_sale_amount" required name="avg_sale_amount">
      </div>
      <div class="col-8"></div>
    </div>
    <div class="form-group row">
      <label for="name" class="col-2 col-form-label">LTV Amt.
        <i class="mytooltip mdi mdi-tooltip-text">
          <span class="tooltip-content customTool text-white">LTV amt - this is how many times someone who purchases from you come back again and purchases again. For example, a dentist might have a sales value of $200. For that person come every six months for 5 years. So the number we would put here would be 10</span>
          </i>
      </label>      
      <div class="col-2">
        <input  class="form-control"  
                type="number" value="0.00" min="0" value="0" step=".01"
                id="ltv_amount" required name="ltv_amount">
      </div>
      <div class="col-8"></div>
    </div>
    <div class="form-group row">
      <label for="cost_con_trgt" class="col-2 col-form-label">Cost / Conv. Target</label>
      <div class="col-2">
        <span>$</span>
        <input  class="form-control"  style="width: 90%"
                type="number" value="0.00" min="0" value="0" step=".01"
                id="cost_con_trgt" required name="cost_con_trgt">
      </div>
      <div class="col-8"></div>
    </div>
  <button type="submit" class="btn btn-success waves-effect waves-light float-right">Submit</button>
</form> -->