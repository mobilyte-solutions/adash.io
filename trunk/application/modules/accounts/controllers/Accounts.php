<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->isLogin();
		$this->load->model('AccountModel');
	}

	public function valid_url($url) {
		$pattern = "/^((ht|f)tp(s?)\:\/\/|~/|/)?([w]{2}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?/";
		if (!preg_match($pattern, $url)) {
			return FALSE;
		}
		return TRUE;
	}

	public function index() {
		$inner = array();
		$shell = array();
		$user_agencies = explode(',', com_user_data('agencies'));
		$inner['accounts'] = $this->AccountModel->getAgencyAccounts($user_agencies);
		$shell['page_title'] = 'Accounts List';
		$shell['content'] = $this->load->view('accounts/index', $inner, true);
		$shell['footer_js'] = $this->load->view('accounts/index_js', $inner, true);
		$this->load->view(TMP_DEFAULT, $shell);
	}

	public function addLogo($account_id) {
		$inner = array();
		$shell = array();
		$user_agencies = explode(',', com_user_data('agencies'));
		$inner['account'] = $this->AccountModel->getAccountDetail($account_id, $user_agencies);
		if ($inner['account']) {
			if (isset($_FILES['report_logo'])) {
				$config['encrypt_name'] = TRUE;
				$config['upload_path'] = 'uploads/report_logo/';
				$config['allowed_types'] = 'gif|jpg|png';
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('report_logo')) {
					$inner['file_error'] = array('error' => $this->upload->display_errors());
				} else {
					$data = array('upload_data' => $this->upload->data());
					$extra['report_logo'] = $data['upload_data']['file_name'];
					$this->AccountModel->updateAccount($inner['account'], $extra);
				}
			}
			$shell['content'] = $this->load->view('accounts/detail', $inner, true);
			$shell['footer_js'] = $this->load->view('accounts/detail_js', $inner, true);
			$this->load->view(TMP_DEFAULT, $shell);
		}
	}

	public function analytics() {
		$inner = array();
		$shell = array();
		$accounts = $this->AccountModel->getProfiles();
		if (!$accounts) {
			redirect('report/ganalyticreport');
			exit;
		}
		com_e($accounts);
		$inner['accounts'] = $accounts;
		$shell['page_title'] = 'Accounts List';
		$shell['content'] = $this->load->view('accounts/analytics', $inner, true);
		$shell['footer_js'] = $this->load->view('accounts/analytics_js', $inner, true);
		$this->load->view(TMP_DEFAULT, $shell);
	}

	public function ulist() {
		$inner = array();
		$shell = array();
		$emsg = $this->session->flashdata('emsg');
		//com_e( $emsg );
		if ($emsg) {
			$inner['emsg'] = $emsg;
		}
		
		$accounts = $this->AccountModel->getProfiles();

		$inner['user_id'] = com_user_data('id');
		$inner['accounts'] = $accounts;
		$shell['page_title'] = 'Accounts Url List';
		$shell['content'] = $this->load->view('accounts/profile_list', $inner, true);
		$shell['footer_js'] = $this->load->view('accounts/profile_list_js', $inner, true);
		$this->load->view(TMP_DEFAULT, $shell);
	}

	public function getAccounts() {
		$out = $inner = array();
		$aProfileId = $this->input->get('aid');
		$aProfileDetail = $this->AccountModel->getFetchedAccountDetail($aProfileId);
		$inner['accounts'] = $this->AccountModel->getAccounts();
		$inner['analytic_profile'] = $aProfileDetail;
		$out['account_html'] = $this->load->view('account_list', $inner, true);
		echo json_encode($out);
		exit();
	}

	public function linkAccount() {
		$account_id = $this->input->post('account_id');
		$analytic_id = $this->input->post('analytic_id');
		$out = array();
		$out['success'] = 0;
		$analyticDetail = $this->AccountModel->getFetchedAccountDetail($analytic_id);
		if ($account_id && $analytic_id && $analyticDetail) {
			$out['success'] = 1;
			$this->AccountModel->linkAnalyticAccount($analytic_id, $account_id);
		}
		echo json_encode($out);
		exit;
	}

	public function updateAccountAdwords($profile_id) {
		$fetchedProfile = $this->AccountModel->getFetchedAccountDetail($profile_id);
		$adwordProj = $this->SocialModel->getAdwordProjDetail($fetchedProfile['linked_adwords_acc_id']);
		if ($fetchedProfile && $fetchedProfile['linked_adwords_acc_id'] && $adwordProj) {
			// rankinity_project_url
			// $anl_prof_id, $adword_prj_id
			$this->AccountModel->updateGoogleAdwordsData($fetchedProfile['id'], $adwordProj);
		}
		redirect('accounts/analytics');
		exit;
	}

	public function updateAccountAnalytic($profile_id) {
		$fetchedProfile = $this->AccountModel->getFetchedAccountDetail($profile_id);
		$adwordProj = $this->SocialModel->getAdwordProjDetail($fetchedProfile['linked_adwords_acc_id']);
		// if ($fetchedProfile && $fetchedProfile[ 'linked_adwords_acc_id' ] && $adwordProj) {
		// 	// rankinity_project_url
		// 	// $anl_prof_id, $adword_prj_id
		// 	$this->AccountModel->updateGoogleAdwordsData($fetchedProfile['id'], $adwordProj);
		// }
		// redirect( 'accounts/analytics' );
		exit;
	}

	public function addProfileUrl() {
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('account_url', 'Account Url', 'required|is_unique[account_url_profiles.account_url]');
		$this->form_validation->set_rules('close_rate', 'Close Rate', 'required');
		$this->form_validation->set_rules('avg_sale_amount', 'Avg. Sale Amt.', 'required');
		$this->form_validation->set_rules('ltv_amount', 'LTV Amt.', 'required');
		$inner = array();
		$shell = array();
		if ($this->form_validation->run() == FALSE) {
			$val_errors = "";
			if ($this->form_validation->error_array()) {
				$val_errors = implode("\n", $this->form_validation->error_array());
			}
			$inner['validation_errors'] = $val_errors;
			$shell['page_title'] = 'Add Account Url';
			$shell['content'] = $this->load->view('accounts/add_prof', $inner, true);
			$shell['footer_js'] = $this->load->view('accounts/add_prof_js', $inner, true);
			$this->load->view(TMP_DEFAULT, $shell);
		} else {
			$this->AccountModel->addProfile();
			redirect('accounts/ulist');
			exit;
		}
	}

	#add_DataForSEO_url
	public function add_DataForSEO_url() {

		$inner = array();
		$shell = array();
		
		$account_id = $this->uri->segment(3);

		$account_details = $this->db->select("*")
			->from('account_url_profiles')
			->where('id', $account_id)
			->get()->row_array();

		//echo "<pre>"; print_r($account_details); echo "</pre>"; die;

		$inner['account_details'] = $account_details;

		$this->form_validation->set_error_delimiters('', '');
		//$this->form_validation->set_rules('site', 'site', 'required|is_unique[data_for_seo_account_profile_keywords.site]');
		$this->form_validation->set_rules('site', 'site', 'required');
		$this->form_validation->set_rules('se_language', 'Search Engine Language', 'required');
		$this->form_validation->set_rules('loc_name_canonical', 'Location Name', 'required');
		$this->form_validation->set_rules('search_keyword', 'Search keyword.', 'required');

		if ($this->form_validation->run() == FALSE) {
			$val_errors = "";
			if ($this->form_validation->error_array()) {
				$val_errors = implode("\n", $this->form_validation->error_array());
			}
			$inner['validation_errors'] = $val_errors;
			$shell['page_title'] = 'Add Data For SEO URL';
			//$shell['account_details'] = $account_details;
			$shell['content'] = $this->load->view('accounts/add_DataForSEO_url', $inner, true);
			$shell['footer_js'] = $this->load->view('accounts/add_prof_js', $inner, true);
			$this->load->view(TMP_DEFAULT, $shell);
		
		} else {

			$insert_row_array = array(
		        'account_url_profile_id' => $this->input->post('account_url_profiles_id'),
		        'site' => $this->input->post('site'),
		        'search_keyword' => $this->input->post('search_keyword')
			);

			$account_details = $this->db->select("id")->from('data_for_seo_account_profile_keywords')
			->where('account_url_profile_id', $account_id)
			->where('site', $this->input->post('site'))
			->where('search_keyword', $this->input->post('search_keyword'))->get()->result_array();

			if ( count($account_details) == 0 ) {
				# code...

				/* ########## ########## */
				include APPPATH . 'third_party/DataForSEO.php';
			
				try {
				    //Instead of 'login' and 'password' use your credentials from https://my.dataforseo.com/login
				    $client = new RestClient('https://api.dataforseo.com/', null, dataforseo_id, dataforseo_password);

				} catch (RestClientException $e) {
				    
				    echo "<pre>";
				    print "HTTP code: {$e->getHttpCode()}\n";
				    print "Error code: {$e->getCode()}\n";
				    print "Message: {$e->getMessage()}\n";
				    print  $e->getTraceAsString();
				    echo "</pre>";
				    exit();
				}

				$post_array = array();

				//for example, some data selection cycle for tasks
				for ($i = 0; $i < 3; $i++) {

				    // example #2 - will return results faster than #1, but is simpler than example #3
				    // All parameters should be set in the text format.
				    // All data will will be searched, compared to our internal parameters
				    // and used as:
				    // "se_id", "loc_id", "key_id" ( actual and fresh list can be found here: "se_id": https://api.dataforseo.com/v2/cmn_se ,
				    // "loc_id": https://api.dataforseo.com/v2/cmn_locations )
				    // If a task was set successfully, this *_id will be returned in results: 'v2/rnk_tasks_post' so you can use it.
				    // The setting of a task can fail, if you set not-existent search engine, for example.
				    // Disadvantages: The process of search and comparison of provided data to our internal parameters may take some time.
				    $my_unq_id = mt_rand(0,30000000); //your unique ID. will be returned with all results

				    $post_array[$my_unq_id] = array(
					    "priority" => 1,
					    "site" => $this->input->post('site'),
					    "se_name" => "google.com",
					    "se_language" => "English",
					    "loc_name_canonical"=> "United States",
					    "key" => mb_convert_encoding( $this->input->post('search_keyword'), "UTF-8")
				    );
				}

				if (count($post_array) > 0) {
				    try {
				        // POST /v2/rnk_tasks_post/$data
				        // $tasks_data must by array with key 'data'
				        $task_post_result = $client->post('v2/rnk_tasks_post', array('data' => $post_array));
				         
				        if ($task_post_result['status'] == 'ok') {

				        	$post_result_count = 0;

				         	if (!empty($task_post_result['results'])) {

				         		foreach ($task_post_result['results'] as $post_result) {
				         			
				         			if ($post_result_count == 0) {

				         				$task_id = $post_result['task_id'];
					         			$insert_row_array['task_id'] = $task_id;
				         			}

					         		$post_result_count++;
				         		}
				         	}
				        }
				        
				        //echo "<pre>"; print_r($task_post_result); echo "</pre>"; echo "<hr>";

				        //do something with post results

				    } catch (RestClientException $e) {
				        
				        echo "<pre>";
				        print "HTTP code: {$e->getHttpCode()}\n";
				        print "Error code: {$e->getCode()}\n";
				        print "Message: {$e->getMessage()}\n";
				        print  $e->getTraceAsString();
				        echo "</pre>";
				    }
				}

				$client = null;

				$insert_id = $this->AccountModel->insert_row_array( $table_name='data_for_seo_account_profile_keywords', $insert_row_array );

				/*
				#1 - get ALL ready results
				recommended use of getting results:
				run this script by cron with 10-60 streams, every minute with random delay 0-30 sec.
				usleep(mt_rand(0,30000000));
				*/
				try {
				    //GET /v2/rnk_tasks_get
				    $task_get_result = $client->get('v2/rnk_tasks_get/'.$task_id);
				    
				    //echo "<pre>"; print_r($task_get_result); echo "</pre>";

				    if ($task_get_result['status'] == 'ok') {
				    	
				    	$organic_result = $task_get_result['results']['organic'][0];				    		
				    	
				    	$insert_keyword_row['month_ref'] 		= '2018-10';
				    	$insert_keyword_row['result_position']  		= $organic_result['result_url'];
				    	$insert_keyword_row['result_se_check_url'] 	= $organic_result['result_se_check_url'];
				    	
				    	$this->AccountModel->insert_row_array( $table_name='data_for_seo_account_keywords_data', $insert_keyword_row );
				    }

				    //do something with results

				} catch (RestClientException $e) {
				    echo "<pre>";
				    print "HTTP code: {$e->getHttpCode()}\n";
				    print "Error code: {$e->getCode()}\n";
				    print "Message: {$e->getMessage()}\n";
				    print  $e->getTraceAsString();
				    echo "<pre>";
				}

				//redirect('accounts/keyword_rank_tracker/'. $insert_id);
				redirect( 'accounts/add_DataForSEO_url/'.$account_id );
				/* ##########/ ########## */

			} else {

				$this->session->set_flashdata('res_status', 'danger'); //danger, success
				$this->session->set_flashdata('res_message', 'The keyword already exits for this domain, Please try to addd another keyword.');

				redirect( 'accounts/add_DataForSEO_url/'.$account_id );
				exit;
			}

			redirect('accounts/ulist');
			exit;
		}
	}

	public function keyword_rank_tracker(){

		echo $account_id = $this->uri->segment(3);

		include APPPATH . 'third_party/DataForSEO.php';

		try {
		    //Instead of 'login' and 'password' use your credentials from https://my.dataforseo.com/login
		    $client = new RestClient('https://api.dataforseo.com', null, 'exe.harish@gmail.com', 'lX4Up2uB7uEAiNzc');
		} catch (RestClientException $e) {
		    echo "<pre>";
		    print "HTTP code: {$e->getHttpCode()}\n";
		    print "Error code: {$e->getCode()}\n";
		    print "Message: {$e->getMessage()}\n";
		    print  $e->getTraceAsString();
		    echo "</pre>";
		}

		/*
		#1 - get ALL ready results
		recommended use of getting results:
		run this script by cron with 10-60 streams, every minute with random delay 0-30 sec.
		usleep(mt_rand(0,30000000));
		*/
		try {
		    //GET /v2/rnk_tasks_get
		    $task_get_result = $client->get('v2/rnk_tasks_get/2000516644');
		    echo "<pre>"; print_r($task_get_result); echo "</pre>";

		    //do something with results

		} catch (RestClientException $e) {
		    echo "<pre>";
		    print "HTTP code: {$e->getHttpCode()}\n";
		    print "Error code: {$e->getCode()}\n";
		    print "Message: {$e->getMessage()}\n";
		    print  $e->getTraceAsString();
		    echo "<pre>";
		}

		die();
	}

	#dataForSEO
	public function dataForSEO() {

		$inner = array();
		$shell = array();

		$shell['page_title'] = 'Data For SEO';
		$shell['content'] = $this->load->view('accounts/dataForSEO', $inner, true);
		$shell['footer_js'] = $this->load->view('accounts/add_prof_js', $inner, true);
		$this->load->view(TMP_DEFAULT, $shell);
	}


	public function editProfileUrl($profId) {
		$profDet = $this->AccountModel->getFetchedAccountDetail($profId);
		$profDetSetting = $this->AccountModel->getFetchedAccountDetailSetting($profId, com_user_data('id'));
		if (!$profDet) {
			redirect('dashboard');
			exit;
		}
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('close_rate', 'Close Rate', 'required');
		$this->form_validation->set_rules('avg_sale_amount', 'Avg. Sale Amt.', 'required');
		$this->form_validation->set_rules('ltv_amount', 'LTV Amt.', 'required');
		$inner = array();
		$shell = array();
		if ($this->form_validation->run() == FALSE) {
			$val_errors = "";
			if ($this->form_validation->error_array()) {
				$val_errors = implode("\n", $this->form_validation->error_array());
			}
			$inner['profDet'] = $profDet;
			$inner['profDetSetting'] = $profDetSetting;
			$inner['validation_errors'] = $val_errors;
			$shell['page_title'] = 'Edit Account url:- ' . $profDet['account_url'];
			$shell['content'] = $this->load->view('accounts/edit_prof', $inner, true);
			$shell['footer_js'] = $this->load->view('accounts/edit_prof_js', $inner, true);
			$this->load->view(TMP_DEFAULT, $shell);
		} else {
			$data = array();
			$data['close_rate'] = $this->input->post('close_rate');
			$data['ltv_amount'] = $this->input->post('ltv_amount');
			$data['cost_con_trgt'] = $this->input->post('cost_con_trgt');
			$data['avg_sale_amount'] = $this->input->post('avg_sale_amount');
			$this->AccountModel->updateProfile($profDet['id'], $data);
			$data = array();
			$data['profile_id'] = $profDet['id'];
			$data['account_id'] = com_user_data('id');
			$seo = $this->input->post('seo');
			$ppc = $this->input->post('ppc');
			$wm = $this->input->post('wm');
			$fr = $this->input->post('fr');
			$data['seo'] = $seo ? json_encode($seo) : json_encode(array());
			$data['ppc'] = $ppc ? json_encode($ppc) : json_encode(array());
			$data['wm'] = $wm ? json_encode($wm) : json_encode(array());
			$data['fr'] = $fr ? json_encode($fr) : json_encode(array());
			if (isset($_FILES['report_logo'])) {
				$config['encrypt_name'] = TRUE;
				$config['upload_path'] = 'uploads/report_logo/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('report_logo')) {
					$inner['file_error'] = array('error' => $this->upload->display_errors());
				} else {
					$fdata = array('upload_data' => $this->upload->data());
					$data['report_logo'] = $fdata['upload_data']['file_name'];
				}
			}
			$where = array();
			$where['profile_id'] = $profDet['id'];
			$where['account_id'] = com_user_data('id');
			$this->AccountModel->updateProfileSetting($where, $data);
			redirect('accounts/ulist');
			exit;
		}
	}

	public function deleteProfileUrl($profId) {
		$profDet = $this->AccountModel->getFetchedAccountDetail($profId);
		if (!$profDet) {
			redirect('dashboard');
			exit;
		}
		$this->AccountModel->removeAccountDetail($profDet['id']);
		redirect('accounts/ulist');
		exit;
	}
}