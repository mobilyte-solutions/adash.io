<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agencies extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->isLogin();
		$this->load->model('AccountModel');
	}

	public function index() {
		$inner = array();
		$shell = array();		
		$inner['agencies'] = $this->AccountModel->getAgencies();
		$shell['page_title'] = 'Agencies List';
		$shell['content'] = $this->load->view('agency_list', $inner, true);
		$shell['footer_js'] = $this->load->view('agency_list_js', $inner, true);
		$this->load->view(TMP_DEFAULT, $shell);
	}

	public function add() {
		$this->form_validation->set_rules( 'agency_name', 'Name', 'required|alpha_numeric_spaces|is_unique[agencies.name]' );
		if( $this->form_validation->run() == false){
			$shell = $inner = array();
			$shell['page_title'] = 'Agency';
			$shell['content'] = $this->load->view('agency_add', $inner, true);
			$shell['footer_js'] = $this->load->view('agency_add_js', $inner, true);
			$this->load->view(TMP_DEFAULT, $shell);
		} else {
			$agency_name = $this->input->post( 'agency_name' );
			$data = array();
			$data[ 'name' ] = $agency_name;
			$data[ 'added_by' ] = com_user_date('id');
			$this->AccountModel->addAgencies($data);
			redirect( 'agencies' );
			exit;
		}
	}

	public function list_user( $agency_id ) {
		$agency = $this->AccountModel->getAgencyData( $agency_id );
		if( !$agency ){
			redirect( 'agencies' );
			exit;
		}
		$agency_users = $this->AccountModel->getAgencyUsers( $agency[ 'id' ] );

		$shell = $inner = array();		
		$inner['agency'] = $agency;		
		$inner['agency_users'] = $agency_users;		
		$shell['page_title'] = 'Agency';
		$shell['content'] = $this->load->view('agency_ulist', $inner, true);
		$shell['footer_js'] = $this->load->view('agency_ulist_js', $inner, true);
		$this->load->view(TMP_DEFAULT, $shell);	
	}

	public function add_user($agency_id) {
		$agency = $this->AccountModel->getAgencyData( $agency_id );
		if( !$agency ){
			redirect( 'agencies' );
			exit;
		}
		$this->form_validation->set_rules( 'username', 'Name', 'trim|required|min_length[5]|max_length[100]|is_unique[users.username]');
		$this->form_validation->set_rules( 'company_name', 'Name', 'trim|required|min_length[5]|max_length[100]');
		$this->form_validation->set_rules( 'email', 'Email', 'trim|required|valid_email|max_length[100]');
		$this->form_validation->set_rules( 'password', 'Password', 'trim|required|max_length[100]');
		if( $this->form_validation->run() == false){
			$shell = $inner = array();
			$inner['agency'] = $agency;
			$shell['page_title'] = 'Agency';
			$shell['content'] = $this->load->view('agency_add_user', $inner, true);
			$shell['footer_js'] = $this->load->view('agency_add_user_js', $inner, true);
			$this->load->view(TMP_DEFAULT, $shell);
		} else {			
			$email = $this->input->post( 'email' );
			$password = $this->input->post( 'password' );
			$username = $this->input->post( 'username' );
			$company_name = $this->input->post( 'company_name' );
			$data = array();			
			$data[ 'email' ] = $email;
			$data[ 'password' ] = $password;
			$data[ 'agencies' ] = $agency[ 'id' ];
			$data[ 'username' ] = $username;
			$data[ 'company_name' ] = $company_name;
			$data['role_id'] = 99;
			$data['is_agency_user'] = '1';
			$data['status'] = '0';	
			if (isset($_FILES['logo'])) {
				$config['encrypt_name'] = TRUE;
				$config['upload_path'] = 'uploads/agency_user/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('logo')) {
					/*echo "error"; die;
					$inner['file_error'] = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('flsh_msg', '<p style="color:#ff0000">'.$this->upload->display_errors().'</p>');
			        redirect('agencies/add_user/'.$agency_id); die;*/
					// return false;
				} else {
					$logoData = array('upload_data' => $this->upload->data());
					$extra['logo'] = $logoData['upload_data']['file_name'];	
					$data['user_image']	= $logoData['upload_data']['file_name'];		
				}
			}
			$userID = $this->AccountModel->addAgencyUser($data);
			if(!empty($userID)){
                 redirect( 'agencies' );
			     exit;
			}else{
               $this->session->set_flashdata('flsh_msg', '<p style="color:#ff0000">Something went wrong! Please try again later.</p>');
			   redirect('agencies/add_user/'.$agency_id); die;     
			}
			
		}
	}
	// public function username_check($str)
 //        {
 //        	$usernames = $this->AccountModel->getallusername();
 //        	if(!empty($usernames)){
 //        		if(in_array($str, $usernames))
 //        	}else{
 //        		return FALSE;
 //        	}
 //                if ($str == 'test')
 //                {
 //                        $this->form_validation->set_message('username_check', 'The {field} field can not be the word "test"');
 //                        return FALSE;
 //                }
 //                else
 //                {
 //                        return TRUE;
 //                }
 //        }
	public function edit_agency_user($agency_id, $userID) {
		$agency = $this->AccountModel->getAgencyData( $agency_id );
		if( !$agency ){
			echo "asdf"; die;
			redirect( 'agencies' );
			exit;
		}
		if( !$userID){
			echo "11"; die;
		    redirect( 'agencies' );
			exit;	
		}
		$userData = $this->AccountModel->getUserData( $userID );
		$this->form_validation->set_rules( 'company_name', 'Name', 'trim|required|min_length[5]|max_length[100]');
		$this->form_validation->set_rules( 'email', 'Email', 'trim|required|valid_email|max_length[100]|is_unique[agency_users.email]');
		if( $this->form_validation->run() == false){
			$shell = $inner = array();
			$inner['agency'] = $agency;
			$inner['userID'] = $userID;
			$inner['userData'] = $userData;
			$shell['page_title'] = 'Agency';
			$shell['content'] = $this->load->view('agency_edit_user', $inner, true);
			$shell['footer_js'] = $this->load->view('agency_add_user_js', $inner, true);
			$this->load->view(TMP_DEFAULT, $shell);
		} else {			
			$email = $this->input->post( 'email' );	
			$username = $this->input->post( 'username' );		
			$company_name = $this->input->post( 'company_name' );
			$user = $this->input->post( 'user' );
			$data = array();			
			$data[ 'email' ] = $email;
			$data[ 'username' ] = $username;
			$data[ 'company_name' ] = $company_name;
			if (!empty($_FILES['logo']['name'])) {
				$config['encrypt_name'] = TRUE;
				$config['upload_path'] = 'uploads/agency_user/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload('logo')) {
					$inner['file_error'] = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('flsh_msg', '<p style="color:#ff0000">'.$this->upload->display_errors().'</p>');
			        redirect('agencies/list_user/'.$agency_id); die;
					// return false;
				} else {
					$logoData = array('upload_data' => $this->upload->data());
					$extra['logo'] = $logoData['upload_data']['file_name'];	
					$data['user_image']	= $logoData['upload_data']['file_name'];		
				}
			}
			$userID = $this->AccountModel->editAgencyUser($user, $data);
			if(!empty($userID)){
                 redirect('agencies/list_user/'.$agency_id); die;     
			     exit;
			}else{
               $this->session->set_flashdata('flsh_msg', '<p style="color:#ff0000">Something went wrong! Please try again later.</p>');
			   redirect('agencies/list_user/'.$agency_id); die;     
			}
			
		}
	}
	public function delete_agency_user($agency_id, $userID) {
		$agency = $this->AccountModel->getAgencyData( $agency_id );
		if( !$agency ){
			redirect( 'agencies' );
			exit;
		}
		if( !$userID){
		    redirect( 'agencies' );
			exit;	
		}
		$userID = $this->AccountModel->deleteAgencyUser($agency_id, $userID);
		if(!empty($userID)){
			 redirect('agencies/list_user/'.$agency_id); die;
		}else{
             $this->session->set_flashdata('flsh_msg', '<p style="color:#ff0000">Something went wrong! Please try again later.</p>');
             redirect('agencies/list_user/'.$agency_id); die;
		}
	}
}