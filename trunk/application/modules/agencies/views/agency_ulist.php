<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-10">
                    </div>
                    <div class="col-lg-2">
                        <a href="<?php echo base_url()?>agencies/add_user/<?=$agency[ 'id' ] ?>" class="btn btn-info">Add Agency User</a>
                    </div>
                </div>
                <div class="table-responsive ">
                    <table id="agencies" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Image</th>                                
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                        <?php
                         if(!empty($agency_users)){
                         foreach( $agency_users as $ag_user){ ?>
                            <tr>
                                <td><?= $ag_user[ 'username' ]; ?></td>
                                <td><?= $ag_user[ 'email' ]; ?></td>
                                <td><?= (!empty($ag_user[ 'company_name' ])) ? $ag_user[ 'company_name' ] : ''; ?></td>
                                <td>
                                    <?php if(!empty($ag_user['user_image']) && file_exists(FCPATH.'uploads/agency_user/'. $ag_user['user_image'])){ ?> <img id="previewImg" src="<?php echo base_url()?>uploads/agency_user/<?php echo $ag_user['user_image'];?>" width="64px" height="44px"><?php }else{ ?>
            <img id="previewImg" src="<?php echo base_url()?>assets/images/users/default.jpeg" width="64px" height="44px">
        <?php } ?>
                                </td>
                                <td><a href="<?php echo base_url()?>agencies/edit_agency_user/<?php echo $agency[ 'id' ].'/'.$ag_user['id']; ?>" class="btn btn-info">Edit</a><a href="javascript:void(0)" class="btn btn-danger" onclick="return confirmation('<?php echo $ag_user['id'];?>')">Delete</a></td>
                            </tr>
                        <?php } } else{ ?>
                        <tr><td colspan="5">No User Found!</td><tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>        
    </div>
</div>
<script type="text/javascript">
    function confirmation(usrid) {
       if(confirm('Are you sure to delete this user?')){
        window.location.href = "<?php echo base_url()?>agencies/delete_agency_user/<?php echo $agency[ 'id' ]?>/"+usrid;
      }else{
        return false;
      }
    }
</script>