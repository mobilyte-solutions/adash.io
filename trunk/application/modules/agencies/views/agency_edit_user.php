<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6 col-8 align-self-center">
                        <h4 class="m-b-0 text-white">Edit a agency user detail</h4>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?php 
                    $errors = validation_errors();
                    if( $errors ){
                        echo ul($errors, array( 'class' => 'form-contrl text-danger' ));
                    }
                   echo $this->session->flashdata('flsh_msg'); 
                ?>
                <form action="<?= base_url( 'agencies/edit_agency_user/'.($agency[ 'id' ]).'/'.$userID  ) ?>" class="form-horizontal" 
                    id="add_agency" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="user" value="<?php echo $userID;?>">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-2">Username</label>
                                    <div class="col-md-10">                                        
                                        <input  type="text" name="username" 
                                                id="username" 
                                                class="form-control" 
                                                required
                                                value="<?php echo (!empty($userData['username']))? $userData['username'] : '';?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-2">Email</label>
                                    <div class="col-md-10">                                        
                                        <input  type="text" name="email" 
                                                value="<?php echo (!empty($userData['email']))? $userData['email'] : '';?>"
                                                id="email" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>                       
                         <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-2">Company Name</label>
                                    <div class="col-md-10">                                        
                                        <input  type="text" name="company_name" 
                                                id="company_name" 
                                                class="form-control" 
                                                required
                                                value="<?php echo (!empty($userData['company_name']))? $userData['company_name'] : '';?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-2">Image</label>
                                    <div class="col-md-5">                                        
                                        <input type="file" name="logo" accept="image/*"  onchange='readURL(this)';>
                                    </div>
                                     <div class="col-md-5">                                        
                                       <?php if(!empty($userData['user_image']) && file_exists(FCPATH.'uploads/agency_user/'. $userData['user_image'])){ ?> <img id="previewImg" src="<?php echo base_url()?>uploads/agency_user/<?php echo $userData['user_image'];?>" width="64px" height="44px"><?php }else{ ?>
                                        <img id="previewImg" src="<?php echo base_url()?>assets/images/users/default.jpeg" width="64px" height="44px">
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                    <hr>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"> </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Row -->
<div id="currentMonthData">
</div>

<div id="lastMonthsData">
</div>
<script type="text/javascript">
  function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
              document.getElementById("previewImg").src = e.target.result;
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
   
</script>