<?php if( !isset($skip_det_link) ){ 
$cssSec = "margin-top:-50px";    
    if( !$show_public_url ){
        $cssSec = "margin-top:0px";
    }
    $cssSec = "";
    ?>
<div class="row logoSection" style="<?= $cssSec; ?>">                
                <div class="col-lg-12 text-right">
                    <?php if( isset( $report_setting[ 'report_logo' ] ) 
                    && $report_setting[ 'report_logo' ]){ 
                        $iattr = array();
                        $iattr[ 'style' ] = ' width="100px" height="60px" ';
                        $iattr[ 'accept' ] = ' image/png,image/gif,image/jpg ';
                        echo img("/uploads/report_logo/".$report_setting[ 'report_logo' ], "", $iattr);
                    } ?>            
                </div>
            </div>
<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-8 col-8 align-self-center">
                        <h4 class="m-b-0 text-white">Board <small><?= $board[ 'board_name' ] ?></small></h4>
                    </div>
                    <div class="col-md-4 col-4 align-self-center">
                        <?php if (1 == 0 && $show_public_url) {
        echo anchor('publicReport/' . $prodDet['share_analytic_link'],
            'Public Link', ' class="btn pull-right btn-outline-primary" target="_blank" ');
    }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
}

    foreach($cardLists as $monthRef => $listDet){
        if( $listDet ){
?>
    <h2>List : <?= $listDet->name ?></h2>
    <div class="row">
        <?php        
            if( isset( $cards ) && $cards  ){
                foreach( $cards as $cIndex => $card ){
                    if( $card->idList == $listDet->id && !$card->closed ){
        ?>
        <div class="col-lg-3 col-md-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title"><?= $card->name; ?></h4>
                    <div class="text-left m-l-10">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <p class="m-b-0 m-t-10"><?= $card->desc ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
                    }
                }
            }
        ?>
        </div>
    <!-- Column -->
<?php 
        }
} ?>