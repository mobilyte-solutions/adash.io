<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crontool extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('adwords');
		$this->load->library('loaddata');
		$this->load->model('ReportModel');
		$this->load->model('AccountModel');
		$this->load->model('SocialappModel');
		$this->loaddata->loadConst();
		$this->load->library('Rankinity', RANKINITY_KEY);
	}

	public function index($to = 'World') {
		echo "Hello {$to}!" . PHP_EOL;
	}

	public function UpdateAccountSocialData() {
		$urlProfiles = $this->db->select('account_url_profiles_social_token.*, account_url_profiles.*,
    							account_url_profiles.profile_id as `analytic_prof_id`,
    							account_url_profiles_social_token.profile_id as `profile_id`')
			->from('account_url_profiles')
			->join('account_url_profiles_social_token',
				'account_url_profiles_social_token.profile_id=account_url_profiles.id', 'left')
			->get()->result_array();
		log_message('debug', 'Social Acc Fetched');
		foreach ($urlProfiles as $urlProfile) {
			log_message('debug', 'URL ID is :'.$urlProfile['id']);
			log_message('debug', 'URL is :'.$urlProfile['account_url']);
			log_message('debug', 'Social Acc Token Update Start');
			$urlProfile = $this->checkTokenValid($urlProfile);
			log_message('debug', 'Social Acc Token Update End');
			$socialAcc = array('mbusiness' => 'gmb',
				'analytic' => 'analytic',
				'adwords' => 'adword',
				'webmaster' => 'gsc',
			);
			foreach ($socialAcc as $socRef => $fldRef) {
				if (!$urlProfile[$fldRef . "_reset_token"]) {
					if ($socRef == 'mbusiness') {
						log_message('debug', 'Crontool');
						log_message('debug', 'Social GMB');
						$fetchedProfile = $this->AccountModel
						->getFetchedAccountDetail($urlProfile['id']);
						if( $fetchedProfile[ 'linked_google_page_location' ] ){
							log_message('debug', 'Social GMB Page Location Data Start');
							$this->SocialappModel->updateProfileGBuissData($fetchedProfile, 2);
							log_message('debug', 'Social GMB Page Location Data End');
						}
					}
					if ($socRef == 'analytic') {
						log_message('debug', 'Crontool');
						log_message('debug', 'Social Analytic');
						$access_token = $urlProfile[$fldRef . "_access_token"];
						$refresh_token = $urlProfile[$fldRef . "_refresh_token"];
						$view_id = $urlProfile["view_id"];
						$prop_id = $urlProfile["property_id"];
						if ($access_token && $refresh_token && $view_id && $prop_id) {
							log_message('debug', 'Crontool');
							log_message('debug', 'Social Analytic Token Updates');
							$opt = array();
							$opt['prod'] = $socRef;
							$opt['profId'] = $urlProfile['id'];
							$opt['log_user_id'] = $urlProfile['account_id'];
							$opt['access_token'] = $access_token;
							$opt['refresh_token'] = $refresh_token;
							$client_token = $this->loaddata->updateGoogleTokens(true, $opt);
							$client = $client_token['client'];
							log_message('debug', 'Social Analytic Data  Start');
							$this->getViewData($urlProfile, $client);
							log_message('debug', 'Social Analytic Data  End');
						}
					}

					if ($socRef == 'adwords') {
						log_message('debug', 'Crontool');
						log_message('debug', 'Social Adwords');
						$access_token = $urlProfile[$fldRef . "_access_token"];
						$refresh_token = $urlProfile[$fldRef . "_refresh_token"];						
						if ($access_token && $refresh_token && $urlProfile["linked_adwords_acc_id"]) {
							log_message('debug', 'Social Adwords Data Start');
							$this->AccountModel
							->updateGoogleAdwordsData($urlProfile, $urlProfile['account_id'], 2);
							log_message('debug', 'Social Adwords Data End');
						}
					}

					if ($socRef == 'webmaster') {
						log_message('debug', 'Crontool');
						log_message('debug', 'Social Webmaster');
						$profDet = $this->AccountModel->getFetchedAccountDetail($urlProfile['id']);
						if( $profDet[ 'linked_webmaster_site' ] ){
							log_message('debug', 'Social Webmaster Data Start');
							$this->ReportModel->updateUrlWebMasterData($profDet, 2);
							log_message('debug', 'Social Webmaster Data End');
						}
					}
							
				}
			}
			log_message('debug', 'Crontool Rankinity Start');
			if ($urlProfile['rankinity_access_token'] &&
				$urlProfile['linked_rankinity_id']) {
				$rankProj = array();
				$rankProj['rankinity_project_id'] = $urlProfile['linked_rankinity_id'];
				$this->AccountModel->linkRankinityAccount($rankProj, $urlProfile['id']);
			}
			log_message('debug', 'Crontool Rankinity End');
		}
	}

	public function checkTokenValid($profDet) {
		$socialAcc = array('mbusiness' => 'gmb',
			'analytic' => 'analytic',
			'adwords' => 'adword',
			'webmaster' => 'gsc',
		);
		$urlProf = $profDet;
		$prof_id = $profDet['id'];
		$account_id = $profDet['account_id'];
		foreach ($socialAcc as $socRef => $fldRef) {
			log_message("debug", $socRef." token update start");
			$email = $urlProf[$fldRef."_email"];
			$access_token = $urlProf[$fldRef."_access_token"];
			$refresh_token = $urlProf[$fldRef."_refresh_token"];
			if ($access_token && $refresh_token) {
				$opt = array();
				$opt['prod'] = $socRef;
				$opt['profId'] = $prof_id;				
				$opt['email'] = $email;
				$opt['log_user_id'] = $account_id;
				$opt['access_token'] = $access_token;
				$opt['refresh_token'] = $refresh_token;
				$ctoken = $this->loaddata->updateGoogleTokens(true, $opt);
				$client = $ctoken['client'];
				$access_token = $client->getAccessToken();
				$refresh_token = $client->getRefreshToken();
				$profDet = array_merge($profDet, $ctoken['profile_detail']);
			}
			log_message("debug", $socRef." token update start");
		}
		$urlProf = array_merge($urlProf, $profDet);
		return $urlProf;
	}

	public function getViewData($profileDet, $gClient) {
		$viewId = $profileDet['view_id'];
		$profileId = $profileDet['analytic_prof_id'];
		$propertyId = $profileDet['property_id'];
		$relProfId = $this->input->post('prof_id');
		$prodDet = $profileDet;
		$client = $gClient;
		$analytics = new Google_Service_Analytics($client);
		$this->ReportModel->fetchPropViewAnalyticData($analytics, $viewId, $prodDet['id'], $prodDet['account_id'], 2);
	}

	public function curlRequest($opts) {
		$url = $opts['url'];
		// create curl resource
		$ch = curl_init();

		// set url
		curl_setopt($ch, CURLOPT_URL, $url);

		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// $output contains the output string
		$output = curl_exec($ch);

		// close curl resource to free up system resources
		curl_close($ch);
		return $output;
		/*
			//API URL
			$url = 'http://www.example.com/api';

			//create a new cURL resource
			$ch = curl_init($url);

			//setup request to send json via POST
			$data = array(
			    'username' => 'codexworld',
			    'password' => '123456'
			);
			$payload = json_encode(array("user" => $data));

			//attach encoded JSON string to the POST fields
			curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

			//set the content type to application/json
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

			//return response instead of outputting
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			//execute the POST request
			$result = curl_exec($ch);

			//close cURL resource
			curl_close($ch);
		*/
	}
}